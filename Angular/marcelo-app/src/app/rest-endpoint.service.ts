import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestEndpointService {

  private input : any;
  private output : any;

  constructor(private http: HttpClient) { }

  public getGreeting() : Observable < any > {
    return this.http.get<any>('http://0.0.0.0:8080/caesarcipher/helloworld');
  }
  
  public getEncryptedString(input : any) : Observable < any > {
    return this.http.post<any>('http://0.0.0.0:8080/caesarcipher/encrypt', input);
  }

  public getDecryptedString(input : any) : Observable < any > {
    return this.http.post<any>('http://0.0.0.0:8080/caesarcipher/decrypt', input);
  }

  public crackCaesarCipher(input : any) : Observable < any > {
    return this.http.post<any>('http://0.0.0.0:8080/caesarcipher/crack', input);
  }

  public setInput(input : any)
  { 
    this.input = input;
  }

  public getInput() : any
  {
    return this.input;
  }

  public setOutput(output : any)
  { 
    this.output = output;
  }

  public getOutput() : any
  {
    return this.output;
  }

}
