import { TestBed } from '@angular/core/testing';

import { RestEndpointService } from './rest-endpoint.service';

describe('RestEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestEndpointService = TestBed.get(RestEndpointService);
    expect(service).toBeTruthy();
  });
});
