import { Component, OnInit, Input } from '@angular/core';
import { RestEndpointService } from '../rest-endpoint.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  public input : any;
  public output : any;
  
  constructor(private restEndpointService : RestEndpointService) { }

  ngOnInit() {
    this.input = this.restEndpointService.getInput();
    this.output = this.restEndpointService.getOutput();
  }

}
