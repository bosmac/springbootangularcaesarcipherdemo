import { Component, OnInit } from '@angular/core';
import { RestEndpointService } from '../rest-endpoint.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  constructor(private restEndpointService : RestEndpointService,
              private router : Router) 
  { }

  ngOnInit() { }

  onClickEncryptButton(string: String)
  {
    console.log("onClickEncryptButton " + string);
    this.restEndpointService.getEncryptedString(string).subscribe(data => {
      console.log(data);
      this.restEndpointService.setInput(string);
      this.restEndpointService.setOutput(data);
      this.router.navigate(['/result']);
    });
  }

  onClickDecryptButton(string: String)
  {
    console.log("onClickDecryptButton " + string);
    this.restEndpointService.getDecryptedString(string).subscribe(data => {
      console.log(data);
      this.restEndpointService.setInput(string);
      this.restEndpointService.setOutput(data);
      this.router.navigate(['/result']);
    });
  }

  onClickCrackButton(string: String)
  {
    console.log("onClickDecryptButton " + string);
    this.restEndpointService.getDecryptedString(string).subscribe(data => {
      console.log(data);
      this.restEndpointService.setInput(string);
      this.restEndpointService.setOutput(data);
      this.router.navigate(['/result']);
    });
  }

}
