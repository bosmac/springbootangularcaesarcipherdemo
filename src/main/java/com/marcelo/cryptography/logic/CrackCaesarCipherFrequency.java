package com.marcelo.cryptography.logic;

import java.util.HashMap;
import java.util.Map;

public class CrackCaesarCipherFrequency {
	
	public final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private int key = -1;
	private String plainTextMessage = "";
	private String encryptedMessage = "";

	public CrackCaesarCipherFrequency(){ }
	
	public String getPlainTextMessage() {
		return plainTextMessage;
	}

	public void setPlainTextMessage(String plainTextMessage) {
		this.plainTextMessage = plainTextMessage;
	}

	public String getEncryptedMessage() {
		return encryptedMessage;
	}

	public void setEncryptedMessage(String encryptedMessage) {
		this.encryptedMessage = encryptedMessage;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public Map<Character,Integer> analyze(String text) 
	{
		Map<Character,Integer> letterFrequencies = new HashMap<>();
		for( int i=0; i<ALPHABET.length(); i++ ){
			letterFrequencies.put( ALPHABET.charAt(i), 0 );
		}		
		for( int i=0; i<text.length(); i++ ) {
			char c = text.charAt(i);		
			if( ALPHABET.indexOf(c) != -1){
				letterFrequencies.put(c, letterFrequencies.get(c)+1);
			}
		}
		return letterFrequencies;
	}
	
	public void decrypt(){
		for(int i=0; i<this.encryptedMessage.length(); i++){
			char character = this.encryptedMessage.charAt(i);
			int encryptedIndex = this.ALPHABET.indexOf(character);
			int alphabetLength = this.ALPHABET.length();
			int plainIndex = Math.floorMod( encryptedIndex-this.key, alphabetLength );
			this.plainTextMessage = this.plainTextMessage + this.ALPHABET.charAt(plainIndex);
		}
	}
	
	public void crack(String encryptedText) 
	{	
		this.encryptedMessage = encryptedText.toUpperCase();
		Map<Character, Integer> letterFrequencies = this.analyze(this.encryptedMessage);
		Map.Entry<Character,Integer> maxEntry = null;
		for (Map.Entry<Character, Integer> entry : letterFrequencies.entrySet()) {
		    if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
		        maxEntry = entry;
		    }
		}		
		char mostFrequentChar = maxEntry.getKey();
		int mostFrequentCharIndex = ALPHABET.indexOf(mostFrequentChar);
		int mostCommonCharIndex = ALPHABET.indexOf(' ');
		int approximatedKey = mostFrequentCharIndex - mostCommonCharIndex;
		this.key = approximatedKey;
		this.decrypt();
	}
	
}
