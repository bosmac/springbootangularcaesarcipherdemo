package com.marcelo.cryptography.logic;

public class CrackCaesarCipher {

	private final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String plainTextMessage = "";
	private String encryptedMessage = "";
	
	public CrackCaesarCipher(){ }
	
	public String getPlainTextMessage() {
		return plainTextMessage;
	}
	public void setPlainTextMessage(String plainTextMessage) {
		this.plainTextMessage = plainTextMessage;
	}
	public String getEncryptedMessage() {
		return encryptedMessage;
	}
	public void setEncryptedMessage(String encryptedMessage) {
		this.encryptedMessage = encryptedMessage;
	}
	
	public void crack(String encryptedText){
		this.setEncryptedMessage(encryptedText.toUpperCase());
		for( int key=0; key<this.ALPHABET.length(); key++ ){
			decrypt(key);
			this.plainTextMessage = this.plainTextMessage + "\n";
		}
	}
	
	public void decrypt(int key){
		for(int i=0; i<this.encryptedMessage.length(); i++){
			char character = encryptedMessage.charAt(i);
			int encryptedIndex = this.ALPHABET.indexOf(character);
			int alphabetLength = this.ALPHABET.length();
			int decryptedIndex = Math.floorMod( encryptedIndex - key, alphabetLength );
			this.plainTextMessage = this.plainTextMessage + this.ALPHABET.charAt(decryptedIndex);
		}
	}
}

