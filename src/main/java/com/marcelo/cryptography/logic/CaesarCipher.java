package com.marcelo.cryptography.logic;

public class CaesarCipher {
	
	private final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private final int key = 5;
	private String plainTextMessage = "";
	private String encryptedMessage = "";
		
	public CaesarCipher() { }
	
	public String getPlainTextMessage() {
		return plainTextMessage;
	}

	public void setPlainTextMessage(String plainTextMessage) {
		this.plainTextMessage = plainTextMessage;
	}

	public String getEncryptedMessage() {
		return encryptedMessage;
	}

	public void setEncryptedMessage(String encryptedMessage) {
		this.encryptedMessage = encryptedMessage;
	}

	public void encrypt(String plainText){
		String encryptedText = "";
		plainText = plainText.toUpperCase();
		for(int i=0; i<plainText.length(); i++){
			char character = plainText.charAt(i);
			int plainIndex = this.ALPHABET.indexOf(character);
			int alphabetLength = this.ALPHABET.length();
			int encryptedIndex = Math.floorMod( plainIndex+this.key, alphabetLength );
			encryptedText = encryptedText + this.ALPHABET.charAt(encryptedIndex);
		}
		this.setPlainTextMessage(plainText);
		this.setEncryptedMessage(encryptedText);
	}
	
	public void decrypt(String encryptedText){
		String plainText = "";
		encryptedText = encryptedText.toUpperCase();
		for(int i=0; i<encryptedText.length(); i++){
			char character = encryptedText.charAt(i);
			int encryptedIndex = this.ALPHABET.indexOf(character);
			int alphabetLength = this.ALPHABET.length();
			int plainIndex = Math.floorMod( encryptedIndex-this.key, alphabetLength );
			plainText = plainText + this.ALPHABET.charAt(plainIndex);
		}
		this.setEncryptedMessage(encryptedText);
		this.setPlainTextMessage(plainText);
	}
	
	
}
