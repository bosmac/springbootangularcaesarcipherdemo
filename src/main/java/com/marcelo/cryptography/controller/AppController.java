package com.marcelo.cryptography.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.marcelo.cryptography.logic.CaesarCipher;
import com.marcelo.cryptography.logic.CrackCaesarCipher;
import com.marcelo.cryptography.logic.CrackCaesarCipherFrequency;
import com.marcelo.cryptography.logic.HelloWorld;

@RestController
@CrossOrigin
@RequestMapping(value = "/caesarcipher")
public class AppController {

	@RequestMapping(value = "/helloworld", method = { RequestMethod.GET, RequestMethod.POST })
	public HelloWorld helloWorld() {
		HelloWorld hw = new HelloWorld();
		hw.setGreeting("Hello World!");
		return hw;
	}

	@RequestMapping(value = "/encrypt", method = { RequestMethod.GET, RequestMethod.POST })
	public CaesarCipher encrypt(@RequestBody String input) {
		CaesarCipher caesarCipher = new CaesarCipher();
		caesarCipher.encrypt(input);
		return caesarCipher;
	}

	@RequestMapping(value = "/decrypt", method = { RequestMethod.GET, RequestMethod.POST })
	public CaesarCipher decrypt(@RequestBody String input) {
		CaesarCipher caesarCipher = new CaesarCipher();
		caesarCipher.decrypt(input);
		return caesarCipher;
	}

	@RequestMapping(value = "/crackbrute", method = { RequestMethod.GET, RequestMethod.POST })
	public CrackCaesarCipher crackbrute(@RequestBody String input) {
		CrackCaesarCipher caesarCipher = new CrackCaesarCipher();
		caesarCipher.crack(input);
		return caesarCipher;
	}
	
	@RequestMapping(value = "/testbrute", method = { RequestMethod.GET, RequestMethod.POST })
	public CrackCaesarCipher testbrute() {
		CrackCaesarCipher caesarCipher = new CrackCaesarCipher();
		String str = ("PMGOLGOHKGHUE OPUNGJVUMPKLU PHSG VGZHEGOLGCYV LGP GPUGJPWOLYG OH GPZGIEGZVGJOHUNPUNG OLGVYKLYGVMG OLGSL  LYZGVMG OLGHSWOHIL G OH GUV GHGCVYKGJVASKGILGTHKLGVA ");
		caesarCipher.crack(str);
		return caesarCipher;
	}
	
	@RequestMapping(value = "/crackfrequency", method = { RequestMethod.GET, RequestMethod.POST })
	public CrackCaesarCipherFrequency crackfrequency(@RequestBody String input) {
		CrackCaesarCipherFrequency caesarCipher = new CrackCaesarCipherFrequency();
		caesarCipher.crack(input);
		return caesarCipher;
	}
	
	@RequestMapping(value = "/testfrequency", method = { RequestMethod.GET, RequestMethod.POST })
	public CrackCaesarCipherFrequency testfrequency() {
		CrackCaesarCipherFrequency caesarCipher = new CrackCaesarCipherFrequency();
		String str = ("PMGOLGOHKGHUE OPUNGJVUMPKLU PHSG VGZHEGOLGCYV LGP GPUGJPWOLYG OH GPZGIEGZVGJOHUNPUNG OLGVYKLYGVMG OLGSL  LYZGVMG OLGHSWOHIL G OH GUV GHGCVYKGJVASKGILGTHKLGVA ");
		caesarCipher.crack(str);
		return caesarCipher;
	}

}